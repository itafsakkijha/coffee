import React from 'react'
import { FaBeer, FaFacebook, FaInstagram, FaTwitter } from 'react-icons/fa';
import {FaPhone} from "react-icons/fa";
import {FaEnvelope} from "react-icons/fa";
import { FaAddressBook } from 'react-icons/fa';
import { FaCoffee } from 'react-icons/fa';
import { FaMugHot } from 'react-icons/fa';
import pic4 from "../assets/images/image5.jpg";
import pic5 from '../assets/images/image6.jpg';
import pic8 from "../assets/images/image8.jpg";




function UiPage() {
  return (
   
    <div className='Uipage-Container'>

      <div className='first-nav'>
        <h1 className='logo'>Coffito <FaMugHot/></h1>


<ul className='Navbar'>
  <li><a href="#showcase">Home</a></li>
  <li><a href="#section-b">Menu</a></li>
  <li><a href="#about">About</a></li>
  <li><a href="#contact">Contact</a></li>
</ul>
</div>
        
      

      <header id="showcase" className="header-pic">
    
     
                <h1 className='coffe-title'>Grap Some Coffee!</h1>
              
             
               
                <a href="#section-b" className="btnnn">Try Coffito</a>
             
               
                </header>

         <div className="Section-onee"  id="section-b">
      

        
      <h1 className="section3-title">Golden Richer, Richer & Smoother..!</h1>
     

      
     <div className="coffee-Section">

      <div className="coffee-one">
      
        <img id="img2" src={pic4}  className="w-100" alt="emp1" />

         <h5 className="coffee-title2">Cappuccino</h5>

          <p className="employee-desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry.  </p>
          
      
      </div>


      <div className="coffee-two">
     
        <img id="img2" src={pic5}  className="w-100 border-bottom" alt="emp1" />
       
          <h5 className="coffee-title2">Golden Coffee </h5>
          <p className="employee-desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry.  </p>
      </div>

      <div className="coffee-three">
      
        <img id="img2" src={pic8} className="w-100" alt="emp1" />
        
          <h5 className="coffee-title2">Turkish Coffee</h5>
          <p className="employee-desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
          </div>
   

       
   </div>

   <div className='coffee-22'>

  

   <div className="coffee-four">
     
     <img id="img2" src={pic5}  className="w-100" alt="emp1" />
    
       <h5 className="coffee-title2">White Mocha </h5>
       <p className="employee-desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry.  </p>
   </div>
   <div className="coffee-four">
     
     <img id="img2" src={pic5}  className="w-100" alt="emp1" />
    
       <h5 className="coffee-title2">Cappuccino </h5>
       <p className="employee-desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry.  </p>
   </div>
    
   <div className="coffee-four">
     
     <img id="img2" src={pic5}  className="w-100" alt="emp1" />
    
       <h5 className="coffee-title2">Coffe Latte </h5>
       <p className="employee-desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry.  </p>
   </div>

          </div>
          
          
          

          <div className='Section-three' id="about">
    <h1 className='story-title'>Our Story</h1>
    <p className='section3-p'>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
     Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
     It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
      It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
   </div>
   


   <section id="section-c" className="grid">
            <div className="content-wrap">
                <h2 className="content-title">Make Someone Happy with a Coffee. <FaCoffee/></h2>
                
            </div>
            
        </section>
        </div>

        <section id="section-d" className="grid">
            <div className="box">
                <div id="content-wrap">
                    <h2 className="content-title">Reach out to us anytime!</h2>

              <h5> <FaAddressBook/> Address: Lebanon-Beirut-Ashrafieh</h5>
              <h5><FaEnvelope/> Email: Support@gmail.com</h5>
              <h5><FaPhone/> phone: 78/000000 - 01000000</h5>
          
              
            
                    
                </div>
            </div>

            <div className="box" id="contact">
                <div id="content-wrap2">
                    <h2 className="content-title">Our Social Network!</h2>

              <h5> <FaFacebook/> Facebook</h5>
              <h5><FaInstagram/> Instagram</h5>
              <h5><FaTwitter/> Twitter</h5>
              </div>
          
              
            
                    
                </div>
         
        </section>

        <footer id="main-footer" className="grid">
            <div className='rights'>Coffee Grounds, All Rights Reserved, 2020</div>
            <div className='rights2'> <a href="http://Youtube.com/c/ZaidIrfanKhan" target="_blank"> Made with <span> &hearts; </span> by Itaf. </a></div>
        </footer>
    

   
      
      




      

                
              
    

      </div>
      

  )
}

export default UiPage
